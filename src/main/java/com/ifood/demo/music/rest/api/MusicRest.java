package com.ifood.demo.music.rest.api;

import com.ifood.demo.dto.TrackRest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/music")
public interface MusicRest {

	@RequestMapping(value = "/getSuggestionsMusic", method = RequestMethod.GET)
	ResponseEntity<TrackRest> getSuggestionsMusic(HttpServletRequest request);


}
