package com.ifood.demo.music.rest.impl;

import com.ifood.demo.dto.CoordinatesDto;
import com.ifood.demo.dto.TrackRest;
import com.ifood.demo.music.rest.api.MusicRest;
import com.ifood.demo.music.services.api.MusicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Service
public class MusicRestImpl implements MusicRest {


	@Autowired
	private MusicService musicService;

	@Override public ResponseEntity<TrackRest> getSuggestionsMusic(HttpServletRequest request) {


		Map<String, String[]> parameters = request.getParameterMap();
		TrackRest trackRest = new TrackRest();

		if(parameters.containsKey("city")){
			trackRest.setTrakcsName(musicService.getPlayListByTemperatureCity(parameters.get("city")[0].toString()));

		}else if(parameters.containsKey("longitude") && parameters.containsKey("latitude")){
			CoordinatesDto coordinatesDto = new CoordinatesDto();
			coordinatesDto.setLongitude(Double.parseDouble(parameters.get("longitude")[0]));
			coordinatesDto.setLatitude(Double.parseDouble(parameters.get("latitude")[0]));

			trackRest.setTrakcsName(musicService.getPlayListByTemperature(coordinatesDto));

		}else {
			throw new RuntimeException();
		}

		return new ResponseEntity(trackRest, HttpStatus.OK);


	}

	@Bean
	public RestTemplate rest(RestTemplateBuilder builder) {
		return builder.build();
	}

}
