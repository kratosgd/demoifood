package com.ifood.demo.music.services.api;

import com.ifood.demo.dto.CoordinatesDto;

import java.util.List;

public interface MusicService {


	List<String> getPlayListByTemperature(CoordinatesDto coordinatesDto);

	List<String> getPlayListByTemperatureCity(String region);


}
