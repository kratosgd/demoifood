package com.ifood.demo.music.services.impl;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.ifood.demo.dto.CoordinatesDto;
import com.ifood.demo.dto.TemperatureDto;
import com.ifood.demo.music.services.api.MusicService;
import com.ifood.demo.utils.ifunction.IFunctionTemperature;
import com.ifood.demo.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.Arrays;
import java.util.List;

@Service
public class MusicServiceImpl implements MusicService{

	private static String PARTY_PLAYLIST_ID ="37i9dQZF1DXd7Zgh168Jxf";
	private static String POP_PLAYLIST_ID ="5uInm2hE4YeoFDPENhLWS9";
	private static String ROCK_PLAYLIST_ID ="37i9dQZF1DWYN0zdqzbEwl";
	private static String CLASSICAL_PLAYLIST_ID ="37i9dQZF1DWV0gynK7G6pD";

	@Autowired
	private Utils utils;

	@Override
	@HystrixCommand(fallbackMethod = "getPlayListByTemperatureDefault")
	public List<String> getPlayListByTemperature(final CoordinatesDto coordinatesDto) {

		TemperatureDto temperatureDto = utils.getTemperatureByCoordinates(coordinatesDto);

		List<String> trackName = getTrackNameSpotify(temperatureDto);

		return trackName;
	}

	@Override
	@HystrixCommand(fallbackMethod = "getPlayListByTemperatureCityDefault")
	public List<String> getPlayListByTemperatureCity(final String city) {

		TemperatureDto temperatureDto =  utils.getTemperatureByCity(city);

		List<String> trackName = getTrackNameSpotify(temperatureDto);

		return trackName;
	}

	private List<String> getTrackNameSpotify(final TemperatureDto temperatureDto) {

		IFunctionTemperature iFunctionTemperature = (temp) -> {
			String playListId  = CLASSICAL_PLAYLIST_ID;

			if(temp > 30){
				playListId =  PARTY_PLAYLIST_ID;
			}else if(temp >= 15 && temp <= 30){
				playListId =  POP_PLAYLIST_ID;
			}else if(temp >= 10 && temp <= 14){
				playListId =  ROCK_PLAYLIST_ID;
			}

			return playListId; };


		List<String> trackName = utils.getTracksNameSpotify(iFunctionTemperature.getPlayListIdTemperature(temperatureDto.getTemperature()));

		return  trackName;



	}

	public List<String> getPlayListByTemperatureDefault(final CoordinatesDto coordinatesDto) {

		List<String> trackName = Arrays.asList("Crimen","Voto Latino","Mr. Bobby","No Dejes Que");

		return  trackName;

	}

	public List<String> getPlayListByTemperatureCityDefault(final String city) {

		List<String> trackName = Arrays.asList("Crimen","Voto Latino","Mr. Bobby","No Dejes Que");

		return  trackName;

	}

}
