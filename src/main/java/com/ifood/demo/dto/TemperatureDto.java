package com.ifood.demo.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class TemperatureDto {

	private Double temperature;
	private Double temperatureMin;
	private Double temperatureMax;

	public Double getTemperature() {
		return temperature;
	}

	public void setTemperature(Double temperature) {
		this.temperature = temperature;
	}

	public Double getTemperatureMin() {
		return temperatureMin;
	}

	public void setTemperatureMin(Double temperatureMin) {
		this.temperatureMin = temperatureMin;
	}

	public Double getTemperatureMax() {
		return temperatureMax;
	}

	public void setTemperatureMax(Double temperatureMax) {
		this.temperatureMax = temperatureMax;
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;

		if (o == null || getClass() != o.getClass())
			return false;

		TemperatureDto that = (TemperatureDto) o;

		return new org.apache.commons.lang3.builder.EqualsBuilder().append(temperature, that.temperature)
					   .append(temperatureMin, that.temperatureMin).append(temperatureMax, that.temperatureMax).isEquals();
	}

	@Override public int hashCode() {
		return new org.apache.commons.lang3.builder.HashCodeBuilder(17, 37).append(temperature).append(temperatureMin)
					   .append(temperatureMax).toHashCode();
	}

	@Override public String toString() {
		return new ToStringBuilder(this).append("temperature", temperature).append("temperatureMin", temperatureMin)
					   .append("temperatureMax", temperatureMax).toString();
	}
}
