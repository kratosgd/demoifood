package com.ifood.demo.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class TrackRest {

	private List<String> trakcs;

	public List<String> getTrakcsName() {
		return trakcs;
	}

	public void setTrakcsName(List<String> trakcs) {
		this.trakcs = trakcs;
	}

	@Override public String toString() {
		return new ToStringBuilder(this).append("trakcs", trakcs).toString();
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;

		if (o == null || getClass() != o.getClass())
			return false;

		TrackRest trackRest = (TrackRest) o;

		return new EqualsBuilder().append(trakcs, trackRest.trakcs).isEquals();
	}

	@Override public int hashCode() {
		return new HashCodeBuilder(17, 37).append(trakcs).toHashCode();
	}


}
