package com.ifood.demo.utils.spotify;

import com.wrapper.spotify.model_objects.specification.PlaylistTrack;

public interface SpotifyUtil {

	PlaylistTrack[]  getPlayList(String playListId);
}
