package com.ifood.demo.utils.spotify;

import com.neovisionaries.i18n.CountryCode;
import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.credentials.ClientCredentials;
import com.wrapper.spotify.model_objects.specification.Playlist;
import com.wrapper.spotify.model_objects.specification.PlaylistTrack;
import com.wrapper.spotify.requests.authorization.client_credentials.ClientCredentialsRequest;
import com.wrapper.spotify.requests.data.playlists.GetPlaylistRequest;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class SpotifyUtilImpl implements SpotifyUtil {

	private static String CLIENT_ID = "343f9cf35f7f4ccaaeb99fecaba408ce";
	private static String CLIENT_SECRET = "f9f70ebe4a2547eb871341f316294117";

	private	static SpotifyApi spotifyApi = new SpotifyApi.Builder().setClientId(CLIENT_ID).setClientSecret(CLIENT_SECRET)
											   .build();

	private static final ClientCredentialsRequest clientCredentialsRequest = spotifyApi.clientCredentials()
																					 .build();


	public PlaylistTrack[]  getPlayList(final String playListId){

		clientCredentials_Sync();

		GetPlaylistRequest getPlaylistRequest = spotifyApi.getPlaylist("eduardo", playListId)
														.market(CountryCode.ES)
														.build();

		Playlist playlist = null;
		playlist = getPlaylist_Sync(getPlaylistRequest, playlist);

		final PlaylistTrack[] playlistTrack =playlist.getTracks().getItems();

		return playlistTrack;
	}

	private Playlist getPlaylist_Sync(GetPlaylistRequest getPlaylistRequest, Playlist playlist) {

		try {
			playlist = getPlaylistRequest.execute();

		} catch (IOException | SpotifyWebApiException e) {
			throw new RuntimeException("Failed : "+e.getMessage());
		}

		return playlist;
	}


	public static void clientCredentials_Sync() {
		try {
			final ClientCredentials clientCredentials = clientCredentialsRequest.execute();

			spotifyApi.setAccessToken(clientCredentials.getAccessToken());

		} catch (IOException | SpotifyWebApiException e) {
			throw new RuntimeException("Failed : "+e.getMessage());
		}
	}


}
