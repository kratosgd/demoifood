package com.ifood.demo.utils.ifunction;

@FunctionalInterface
public interface IFunctionTemperature {

	String getPlayListIdTemperature(Double temperature);
}
