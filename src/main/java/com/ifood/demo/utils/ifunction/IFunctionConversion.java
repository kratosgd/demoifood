package com.ifood.demo.utils.ifunction;

@FunctionalInterface
public interface IFunctionConversion {

	Double fahrenheitToCelsius(Double temperature);
}
