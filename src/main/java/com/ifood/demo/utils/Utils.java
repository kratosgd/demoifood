package com.ifood.demo.utils;

import com.ifood.demo.dto.CoordinatesDto;
import com.ifood.demo.dto.TemperatureDto;
import com.ifood.demo.utils.ifunction.IFunctionConversion;
import com.ifood.demo.utils.spotify.SpotifyUtil;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class Utils {

	private static String RESPONSE_OK = "200";
	private static String OPEN_WEATHERMAP_API_KEY = "b4a9dc8a3739ab7d61661cfaf440bca9";

	private static final Logger LOGGER = LoggerFactory.getLogger(Utils.class);


	@Autowired
	private SpotifyUtil spotifyUtil;

	public TemperatureDto getTemperatureByCoordinates(CoordinatesDto coordinatesDto){

		StringBuilder url = new StringBuilder("http://api.openweathermap.org/data/2.5/weather?lat=");
		url.append(coordinatesDto.getLatitude().toString()).append("&lon=").append(coordinatesDto.getLongitude().toString()).
					append("&appid=").append(OPEN_WEATHERMAP_API_KEY);

		JSONObject jsonObject = restClient(url);

		TemperatureDto temperatureDto = mapJsonObjectToTemperatureDto(jsonObject);

		LOGGER.info(temperatureDto.toString());
		return temperatureDto;
	}

	public TemperatureDto getTemperatureByCity(final String region){
		StringBuilder url = new StringBuilder("http://api.openweathermap.org/data/2.5/find?q=");
		url.append(region).append("&units=metric").append("&appid=").append(OPEN_WEATHERMAP_API_KEY);

		JSONObject jsonObject = restClient(url);

		TemperatureDto temperatureDto = mapJsonObjectToTemperatureDto(jsonObject);

		LOGGER.info(temperatureDto.toString());
		return temperatureDto;
	}

	@Cacheable("tracks")
	public List<String> getTracksNameSpotify(final String playListId) {

		List<String> trackName = new ArrayList<>(0);

		Arrays.stream(spotifyUtil.getPlayList(playListId)).forEach(track -> trackName.add(track.getTrack().getName()));

		return trackName;
	}

	private TemperatureDto mapJsonObjectToTemperatureDto(JSONObject jsonObject) {
		TemperatureDto temperatureDto = new TemperatureDto();

		if(RESPONSE_OK.equals(jsonObject.get("cod").toString())){
			JSONObject mainJsonObject;

			if(jsonObject.isNull("main")){
				mainJsonObject = (JSONObject) ((JSONObject)jsonObject.getJSONArray("list").get(0)).get("main");


			}else{
				mainJsonObject = (JSONObject)jsonObject.get("main");
			}

			IFunctionConversion conversion = (temp) -> (temp - 273.15);

			temperatureDto.setTemperature(conversion.fahrenheitToCelsius(mainJsonObject.getDouble("temp")));
			temperatureDto.setTemperatureMax(conversion.fahrenheitToCelsius(mainJsonObject.getDouble("temp_max")));
			temperatureDto.setTemperatureMin(conversion.fahrenheitToCelsius(mainJsonObject.getDouble("temp_min")));

		}else{
			throw new RuntimeException("Failed : HTTP error code : "
											   + jsonObject.getString("cod"));
		}

		return temperatureDto;
	}

	private JSONObject restClient(StringBuilder url) {

		JSONObject jsonObject;

		try{
			RestTemplate restTemplate = new RestTemplate();

			String result = restTemplate.getForObject(url.toString(),String.class);
			jsonObject = new JSONObject(result);


		}catch (RestClientException e){
			throw new RuntimeException("Failed : "+e.getMessage());
		}

		return  jsonObject;
	}




}

