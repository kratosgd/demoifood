package com.ifood.demo.utils;


import com.ifood.demo.dto.CoordinatesDto;
import com.ifood.demo.dto.TemperatureDto;
import com.ifood.demo.utils.spotify.SpotifyUtil;
import com.ifood.demo.utils.spotify.SpotifyUtilImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNotNull;


import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class UtilsTest {


	@InjectMocks
	private Utils utils;

	@Spy
	private SpotifyUtil spotifyUtil = new SpotifyUtilImpl();


	@Test
	public void getTemperatureByCoordinates (){

		CoordinatesDto coordinatesDto = new CoordinatesDto();
		coordinatesDto.setLatitude(Double.parseDouble("35"));
		coordinatesDto.setLongitude(Double.parseDouble("139"));

		TemperatureDto temperatureDto = utils.getTemperatureByCoordinates(coordinatesDto);

		assertNotNull(temperatureDto);
		assertNotNull(temperatureDto.getTemperature());

	}

	@Test
	public void getTracksNameSpotify(){

		List<String> trackNames = utils.getTracksNameSpotify("37i9dQZF1DXd7Zgh168Jxf");

		trackNames.stream().forEach(track -> { System.out.println(track +"\n");
				assertNotNull(trackNames);});

	}



}
